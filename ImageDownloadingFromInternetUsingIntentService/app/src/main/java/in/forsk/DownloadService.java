package in.forsk;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadService extends IntentService {

	public static final int STATUS_RUNNING = 0;
	public static final int STATUS_FINISHED = 1;
	public static final int STATUS_ERROR = 2;

	private static final String TAG = "DownloadService";

	public DownloadService() {
		super(DownloadService.class.getName());
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Log.d(TAG, "Service Started!");

		// final ResultReceiver receiver =
		// intent.getParcelableExtra("receiver");
		String url = intent.getStringExtra("url");
		
		//Creating intent for the intent filter which is register with our activity
		Intent i = new Intent("in.forsk.update");
		
		//kind of hashmap (Key value pairs) to hold the data (parceable)
		Bundle bundle = new Bundle();

		if (!TextUtils.isEmpty(url)) {
			/* Update UI: Download Service is Running */

			//Parceable concept is use to exchange the data between the class(through intent)
			i.putExtras(Bundle.EMPTY);
			i.putExtra("resultCode", STATUS_RUNNING);
			//Send Broadcast to out activity by intent i
			sendBroadcast(i);
			
			notify("Downlaoding..");

			try {
				InputStream in = Utils.openHttpConnection(url);
				Bitmap bmp = BitmapFactory.decodeStream(in);

				//Save Bitmap
				File image_file =  new File(getExternalCacheDir(),"temp.png");
				FileOutputStream outStream = new FileOutputStream(image_file);

				bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);

				outStream.flush();
				outStream.close();

				/* Sending result back to activity */
				if (bmp != null) {
					bundle.putString("result", image_file.getPath());
					// receiver.send(STATUS_FINISHED, bundle);
					i.putExtras(bundle);
					i.putExtra("resultCode", STATUS_FINISHED);
					//Send Broadcast to out activity by intent i
					sendBroadcast(i);

					notify("Downlaoding finished, File is ready to use");
				}
			} catch (Exception e) {

				e.printStackTrace();

				/* Sending error message back to activity */
				bundle.putString(Intent.EXTRA_TEXT, e.toString());
				// receiver.send(STATUS_ERROR, bundle);
				i.putExtras(bundle);
				i.putExtra("resultCode", STATUS_ERROR);
				//Send Broadcast to out activity by intent i
				sendBroadcast(i);
				notify("Downlaoding finished with error " + e.toString());
			}
		}
		Log.d(TAG, "Service Stopping!");
		this.stopSelf();
	}

	public String downloadFile(String urlString) throws Exception {
		URL url = new URL(urlString);
		URLConnection connection = url.openConnection();
		connection.connect();
		// this will be useful so that you can show a typical 0-100%
		// progress bar
		int fileLength = connection.getContentLength();

		File f = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "test.pdf");
		if (!f.exists())
			f.createNewFile();

		// download the file
		InputStream input = new BufferedInputStream(url.openStream());
		OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + File.separator + "test.pdf");

		byte data[] = new byte[1024];
		long total = 0;
		int count;
		while ((count = input.read(data)) != -1) {
			total += count;
			output.write(data, 0, count);
		}

		output.flush();
		output.close();
		input.close();
		return Environment.getExternalStorageDirectory().getPath() + File.separator + "test.pdf";
	}

	private void notify(String methodName) {
		String name = this.getClass().getName();
		String[] strings = name.split("\\.");
		Notification noti = new Notification.Builder(this).setContentTitle(methodName).setAutoCancel(true).setSmallIcon(R.drawable.ic_launcher).setContentText(name).build();
		// Comment by sorbh
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify((int) System.currentTimeMillis(), noti);
	}

	public class DownloadException extends Exception {

		public DownloadException(String message) {
			super(message);
		}

		public DownloadException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}